<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Libs\Comments;

class CommentsAPIController extends Controller
{
    public function list(Request $request){
        $postId = $request->input('postId') ?: null;
        $name = $request->input('name') ?: null;
        $email = $request->input('email') ?: null;
        $body = $request->input('body') ?: null;

        $comments = (new Comments)->comments;

        return $comments->when($postId,function($comment) use ($postId){
            return $comment->where('postId',$postId);
        })->when($name,function($comment) use ($name){
            return $comment->filter(function($string) use ($name){
                return stripos($string['name'],$name) !== false;
            });
        })->when($body,function($comment) use ($body){
            return $comment->filter(function($string) use ($body){
                return stripos($string['body'],$body) !== false;
            });
        })->when($email,function($comment) use ($email){
            return $comment->filter(function($string) use ($email){
                return stripos($string['email'],$email) !== false;
            });
        });
    }
}
