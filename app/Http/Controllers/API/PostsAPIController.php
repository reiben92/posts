<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Libs\Posts;

class PostsAPIController extends Controller
{
    public function list(){
        return $posts = (new Posts())->posts;
    }

    public function get($post_id){
        return $posts = (new Posts())->posts->where('post_id',$post_id);
    }
}
