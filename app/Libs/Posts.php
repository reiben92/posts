<?php

namespace App\Libs;

use Illuminate\Support\Facades\Http;

class Posts
{
    public $posts = array();

    public function __construct(){
        $this->getPosts();
        $this->mapFields();
    }

    public function getPosts(){
        $response = Http::get('https://jsonplaceholder.typicode.com/comments');
        $this->posts = $response->collect();
    }

    public function mapFields(){
        $collection = collect($this->posts)->map(function($post){
            $map['post_id'] = $post['postId'];
            $map['post_title'] = $post['name'];
            $map['post_body'] = $post['body'];
            $map['comments'] = $this->posts->where('postId',$post['postId'])->count();
            return $map;
        });

        $this->posts = $collection;
    }
    
}
