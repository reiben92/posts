<?php

namespace App\Libs;

use Illuminate\Support\Facades\Http;

class Comments
{
    public $comments = array();

    public function __construct(){
        $this->getComments();
    }

    public function getComments(){
        $response = Http::get('https://jsonplaceholder.typicode.com/comments');
        $this->comments = $response->collect();
    }
    
}
